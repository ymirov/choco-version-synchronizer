# Package synchronizer

## Description
Package synchronizer is a combination of PowerShell scripts that access all _choco-installed_ software and all the software in _Programs and Features_ and compares the versions of every program. If they don't match, then that means that the app has been updated (after the choco install) and will be manually upgraded/updated using choco.
## Architecture Diagram
![alt text](./Diagram.png "")
DBManager.ps1 is responsible for creating a sqlite database to store the applications from _Programs and Features_ and another sqlite database that stores all the choco-installed packages.
## Purpose
This package synchronizer's purpose is to accurately display the currently-installed version of every app enlisted in the CERN AppStore by running periodic checks and detect if a manual/external-from-appstore update has occurred.
## How it works
#### PeriodicExecutor.ps1
Responsible for periodically executing the main script (DBManager.ps1). The last line in this script is responsible for setting the amount of time between executions. 
#### DBManager.ps1
Responsible for the comparisons between choco and programs and features versions and updating choco packages if required. It also exports the versions into their own sqlite .db files - one for choco installed package versions and one for Programs and Features.
#### Synchronizer.ps1
Responsible for the comparisons between choco and programs and features versions and updating choco packages if required. It does not export the versions to .db files.
## How to use
For periodic synchronization, simply run PeriodicExecutor.ps1 and change the last line in the script to specify how often you would want the checks to occur.
For a single synchronization, simply run DBManager.ps1 if you want the versions exported to .db files or Synchronizer.ps1 if you don't want them exported.
### Periodic frequence trade-offs
##### Only when the client starts
If we decide to only do one check per client, that would mean that we won't lose any performance because the synchronization will be carried out only once. However, the problem with this approach is that if the user doesn't shut down the client (for example they never shut down their computer), the synchronization will lack behind and possibly never be carried out.
##### Once per day
This approach will not affect the performance and also promises at least one synchronization per day (unlike the above-mentioned approach). However, a technical challenge would be to decide when exactly is the best time to carry out the synchronization. For example, if it is a fixed time, (let's say 5PM), how do we know that the user has not shut down the computer? Because if that's the case, the synchronization wouldn't be carried out. Hence, both this and the previous approaches have a single point of underperformance.
##### Once every few hours
This approach  seems to be the most promising one. For one, it ensures that the synchronization would be carried out frequently and thus the correct version would be displayed in the appstore. And second, even though it would require more performance, it wouldn't affect it tremendously since the scripts are not heavy.
##### Summary
Once every few hours seems to be the most promising approach because of its frequency. The other two methods are very likely to not run the synchronization scripts often enough and that would lead to the user not seeing the correct installed app version.
## CERN AppStore Service Integration
The C# client gives us a lot of useful functionality that lets us achieve what we can with the ps1 scripts.
We can IApiServices.GetInstalledApps() and IApiServices.GetOutdatedPackages() to get all the installed packages 
and compare them with the Programs and Features versions. 
If there is a need for synchronization (i.e. updating the choco version), we can run ChocoService.UpdatePackage() to 
update the choco package version.
For functionality that is hard to be achieved using C# and that can be easily done with PowerShell, .NET allows us to
execute PowerShell scripts within C#. We can utilize that functionality to access the apps in Programs and Features for example.
## Test
I installed notepad++ from CERN AppStore and its version is 7.7.1. Then I opened notepad++ and it wanted to update from its internal installer.
Then when I updated, I ran the synchronizer and it updated the CERN AppStore notepad++ version accordingly by upgrading choco app version directly..
### TODO
-Integrate with C# Client