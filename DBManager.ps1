
Import-Module PSSQLite
$Database = ".\db\apps.SQLite";
$Query = "CREATE TABLE apps (
    name VARCHAR(255) NOT NULL,
    version VARCHAR(255) NOT NULL
)";
Invoke-SqliteQuery -Query $Query -DataSource $Database;
Invoke-SqliteQuery -DataSource $Database -Query "PRAGMA table_info(apps)";

$Database_2 = ".\db\programsAndFeatures.SQLite";
$Query = "CREATE TABLE programs (
    name VARCHAR(255) PRIMARY KEY NOT NULL,
    version VARCHAR(255) NOT NULL
)";
Invoke-SqliteQuery -DataSource $Database_2 -Query $Query;
Invoke-SqliteQuery -DataSource $Database_2 -Query "PRAGMA table_info(programs)";

$InstalledSoftware = Get-ChildItem "HKLM:\Software\Microsoft\Windows\CurrentVersion\Uninstall";

# Insert into apps all applications installed with chocolatey
$chocoPrograms = choco list -l;
for($i = 0; $i -lt $chocoPrograms.length -1; $i++) {
    $programName = '';
    $stoppedIndex = -1;
    for ($j = 0; $j -lt $chocoPrograms[$i].length; $j++) {
        if ($chocoPrograms[$i][$j] -ne ' ') {
            $programName = $programName + $chocoPrograms[$i][$j];
        } else {
            $stoppedIndex = $j;
            break;
        };
    };

    $programVersion = '';
    for ($j = $stoppedIndex+1; $j -lt $chocoPrograms[$i].length; $j++) {
        if ($chocoPrograms[$i][$j] -ne 'v') {
            $programVersion = $programVersion + $chocoPrograms[$i][$j];
        };
    };
    
    foreach($obj in $InstalledSoftware) {
        $programName_2 = $obj.GetValue('DisplayName');
        $programVersion_2 = $obj.GetValue('DisplayVersion');
        
        if ($programName_2) {
            $temp_2 = $programName_2.toLower() -replace '[\W0-9]', '';
            $temp = $programName.toLower() -replace '[\W0-9]', '';

            if ($programName -eq 'notepadplusplus.install') {
                $temp = 'notepad';
            };

            if ($temp_2 -Match $temp) {
                # Check for version equality
                if ($programVersion -ne $programVersion_2) {
                    choco upgrade $programName --version $programVersion_2 -y;
                };

                $insertQuery = "INSERT INTO programs (name, version) VALUES (@pName, @pVersion);";
                Invoke-SqliteQuery -DataSource $Database_2 -Query $insertQuery -SqlParameters @{
                    pName = $programName
                    pVersion = $programVersion_2
                };
            };
        };
        
    };

    $insertQuery = "INSERT INTO apps (name, version) VALUES (@pName, @pVersion);"

    Invoke-SqliteQuery -DataSource $Database -Query $insertQuery -SqlParameters @{
        pName = $programName
        pVersion = $programVersion
    };
};


Invoke-SqliteQuery -DataSource $Database_2 -Query "SELECT * FROM programs";
Invoke-SqliteQuery -DataSource $Database_2 -Query "DROP TABLE programs";
Invoke-SqliteQuery -DataSource $Database -Query "DROP TABLE apps";
