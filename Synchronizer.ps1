
Import-Module PSSQLite

$InstalledSoftware = Get-ChildItem "HKLM:\Software\Microsoft\Windows\CurrentVersion\Uninstall";

# Insert into apps all applications installed with chocolatey
$chocoPrograms = choco list -l;
for($i = 0; $i -lt $chocoPrograms.length -1; $i++) {
    $chocoPackageName = '';
    $stoppedIndex = -1;
    for ($j = 0; $j -lt $chocoPrograms[$i].length; $j++) {
        if ($chocoPrograms[$i][$j] -ne ' ') {
            $chocoPackageName = $chocoPackageName + $chocoPrograms[$i][$j];
        } else {
            $stoppedIndex = $j;
            break;
        };
    };

    $chocoPackageVersion = '';
    for ($j = $stoppedIndex+1; $j -lt $chocoPrograms[$i].length; $j++) {
        if ($chocoPrograms[$i][$j] -ne 'v') {
            $chocoPackageVersion = $chocoPackageVersion + $chocoPrograms[$i][$j];
        };
    };
    
    foreach($obj in $InstalledSoftware) {
        $programName = $obj.GetValue('DisplayName');
        $programVersion = $obj.GetValue('DisplayVersion');
        
        if ($programName) {
            $temp_2 = $programName.toLower() -replace '[\W0-9]', '';
            $temp = $chocoPackageName.toLower() -replace '[\W0-9]', '';

            if ($chocoPackageName -eq 'notepadplusplus.install') {
                $temp = 'notepad';
            };

            if ($temp_2 -Match $temp) {
                # Check for version equality
                if ($chocoPackageVersion -ne $programVersion) {
                    choco upgrade $chocoPackageName --version $programVersion -y;
                };
            };
        };
    };
};

choco list -l;
